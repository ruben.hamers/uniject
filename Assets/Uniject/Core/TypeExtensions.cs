using System;
using System.Linq;

namespace HamerSoft.Uniject.Core
{
    public static class TypeExtensions
    {
        
        public static bool IsGenericTypeSubClassOrDerivableFrom(this Type type, Type baseType)
        {
            if (type == null || baseType == null || type == baseType)
                return false;

            if (!baseType.IsGenericType && !type.IsGenericType)
                return type.IsSubclassOf(baseType) || baseType.IsAssignableFrom(type);

            baseType = baseType.GetGenericTypeDefinition();
            type = type.BaseType;
            Type objectType = typeof(object);

            bool DoesImplementGenericInterface(Type targetType)
            {
                return targetType.GetInterfaces()
                    .Where(i => i.IsGenericType)
                    .Where(i => i.GetGenericTypeDefinition() == baseType)
                    .Select(i => i.GetGenericArguments().First())
                    .FirstOrDefault()!= null;
            }

            while (type != objectType && type != null)
            {
                Type currentType = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
                if (currentType == baseType || DoesImplementGenericInterface(currentType) )
                    return true;

                type = type.BaseType;
            }

            return false;
        }
    }
}