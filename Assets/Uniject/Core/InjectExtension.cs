using System;
using System.Collections.Generic;
using System.Reflection;
using HamerSoft.Uniject.Core.Injectors;
using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Core
{
    public static class InjectExtension
    {
        private static Injector _injector;
        private static Constructor _constructor;
        private static Invoker _invoker;
        private static Dictionary<Type, object> _staticInstances;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void CreateInjector()
        {
            if (_injector != null)
                return;

            _staticInstances = new Dictionary<Type, object>();
            _constructor = new Constructor(_staticInstances);
            _injector = new Injector(_staticInstances);
            _invoker = new Invoker(_staticInstances);
            QuitHandler.Spawn(Clear);
        }

        private static void Clear()
        {
            _injector?.Dispose();
            _constructor?.Dispose();
            _invoker?.Dispose();
            _injector = null;
            _constructor = null;
            _invoker = null;
        }

        #region Field Injection

        public static void Inject(this object injectable)
        {
            _injector.Inject(injectable);
        }

        #endregion

        #region Constructor Injection

        public static T1 Construct<T1, T2>(this object instance) where T2 : IResolver
        {
            return _constructor.Construct<T1, T2>();
        }

        public static void Construct<T1, T2>(this object instance, Action<T1> callack) where T2 : IResolver<T1>
        {
            _constructor.Construct<T1, T2>(callack);
        }

        #endregion

        #region MethodInjection

        public static void Invoke<T>(this object reference, Action<T> method)
            where T : new()
        {
            _invoker.Invoke(reference, method);
        }

        public static void Invoke<T1, T2>(this object reference, Action<T1, T2> method)
            where T1 : new()
            where T2 : new()
        {
            _invoker.Invoke(reference, method);
        }

        public static void Invoke<T1, T2, T3>(this object reference, Action<T1, T2, T3> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
        {
            _invoker.Invoke(reference, method);
        }

        public static void Invoke<T1, T2, T3, T4>(this object reference, Action<T1, T2, T3, T4> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
            where T4 : new()
        {
            _invoker.Invoke(reference, method);
        }

        public static TReturn Invoke<T, TReturn>(this object reference, Func<T, TReturn> method) where T : new()
        {
            return _invoker.Invoke(reference, method);
        }

        public static TReturn Invoke<T1, T2, TReturn>(this object reference, Func<T1, T2, TReturn> method)
            where T1 : new()
            where T2 : new()
        {
            return _invoker.Invoke(reference, method);
        }

        public static TReturn Invoke<T1, T2, T3, TReturn>(this object reference, Func<T1, T2, T3, TReturn> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
        {
            return _invoker.Invoke(reference, method);
        }

        public static TReturn Invoke<T1, T2, T3, T4, TReturn>(this object reference,
            Func<T1, T2, T3, T4, TReturn> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
            where T4 : new()
        {
            return _invoker.Invoke(reference, method);
        }

        #endregion

        #region Utilities

        internal static IResolver CreateResolver(this Type resolver)
        {
            if (resolver.GetConstructor(Type.EmptyTypes) != null)
                return Activator.CreateInstance(resolver) as IResolver;
            else
                return Activator.CreateInstance(resolver,
                    BindingFlags.CreateInstance |
                    BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance |
                    BindingFlags.OptionalParamBinding,
                    null, new System.Object[] {Type.Missing}, null) as IResolver;
        }
        
        internal static Resolver CreateComponentResolver(this Type resolver, Type memberType)
        {
            if (resolver.GetConstructor(Type.EmptyTypes) != null)
                return (Resolver) Activator.CreateInstance(resolver);
            else
                return (Resolver) Activator.CreateInstance(resolver,
                    BindingFlags.CreateInstance |
                    BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance |
                    BindingFlags.OptionalParamBinding,
                    null, new System.Object[] {Type.Missing}, null);
        }

        internal static IResolver<T> CreateResolver<T>(this Type resolver)
        {
            if (resolver.GetConstructor(Type.EmptyTypes) != null)
                return (IResolver<T>) Activator.CreateInstance(resolver);
            else
                return (IResolver<T>) Activator.CreateInstance(resolver,
                    BindingFlags.CreateInstance |
                    BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance |
                    BindingFlags.OptionalParamBinding,
                    null, new System.Object[] {Type.Missing}, null);
        }

#if UNITY_EDITOR
        public static void Reset()
        {
            if (Application.isPlaying)
            {
                Debug.LogError(
                    "You are forcing a reset of the Injection container at run-time. This is not allowed! Reset is an editor only function!");
                return;
            }

            Clear();
            CreateInjector();
        }
#endif

        #endregion
    }
}