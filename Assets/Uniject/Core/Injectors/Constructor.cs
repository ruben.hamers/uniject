using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;
using ConstructorInfo = HamerSoft.Uniject.Core.Info.ConstructorInfo;

namespace HamerSoft.Uniject.Core.Injectors
{
    internal class Constructor : Unijector
    {
        private Dictionary<Type, ConstructorInfo> _constructorInfos;

        public Constructor(Dictionary<Type, object> staticReferences) : base(staticReferences)
        {
            _constructorInfos = new Dictionary<Type, ConstructorInfo>();
        }

        internal T1 Construct<T1, T2>() where T2 : IResolver
        {
            AddConstructorInfo<T1>();
            return (T1) Construct<T2>(typeof(T1), _constructorInfos[typeof(T1)]);
        }

        internal void Construct<T1, T2>(Action<T1> callback) where T2 : IResolver<T1>
        {
            AddConstructorInfo<T1>();
            var resolver = CreateCallbackResolver<T1, T2>(_constructorInfos[typeof(T1)]);
            resolver.Resolve(callback);
        }

        private void AddConstructorInfo<T1>()
        {
            if (!_constructorInfos.ContainsKey(typeof(T1)))
                _constructorInfos.Add(typeof(T1),
                    new ConstructorInfo(typeof(T1).GetConstructors(
                        BindingFlags.Instance |
                        BindingFlags.Public |
                        BindingFlags.NonPublic |
                        BindingFlags.Static |
                        BindingFlags.FlattenHierarchy)));
        }

        private object Construct<T>(Type type, ConstructorInfo constructor) where T : IResolver
        {
            var resolver = CreateResolver<T>(constructor);
            if (resolver == null)
            {
                Debug.LogWarning(
                    $"No Constructor found on type {type} with resolver {typeof(T)}! Could not instantiate!");
                return null;
            }

            return resolver.Resolve();
        }

        private IResolver CreateResolver<T>(ConstructorInfo constructor)
        {
            if (CreateResolver<T>(constructor.Methods.Keys.ToArray(), out var resolver))
                return resolver;

            return null;
        }

        private T2 CreateCallbackResolver<T1, T2>(ConstructorInfo constructor)
        {
            if (CreateCallbackResolver<T1, T2>(constructor.Methods.Keys.ToArray(), out var resolver))
                return resolver;

            return default;
        }

        private bool CreateResolver<T>(System.Reflection.ConstructorInfo[] constructors, out IResolver result)
        {
            foreach (var info in constructors)
            {
                Type resolver = null;
                if ((resolver = info.GetCustomAttribute<AbstractInjector>()?.GetResolver()) == null ||
                    resolver != typeof(T))
                    continue;

                result = resolver.CreateResolver();
                return true;
            }

            result = null;
            return false;
        }

        private bool CreateCallbackResolver<T1, T2>(System.Reflection.ConstructorInfo[] constructors, out T2 result)
        {
            foreach (var info in constructors)
            {
                Type resolver = null;
                if ((resolver = info.GetCustomAttribute<AbstractInjector>()?.GetResolver()) == null ||
                    resolver != typeof(T2))
                    continue;

                result = (T2) resolver.CreateResolver<T1>();
                return true;
            }

            result = default;
            return false;
        }
        
        public void Dispose()
        {
            _constructorInfos = null;
        }
    }
}