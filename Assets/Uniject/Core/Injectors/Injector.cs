using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Uniject.Core.Attributes;
using UnityEngine;
using MemberInfo = HamerSoft.Uniject.Core.Info.MemberInfo;

namespace HamerSoft.Uniject.Core.Injectors
{
    internal class Injector : Unijector
    {
        private Dictionary<Type, MemberInfo> _typeTable;

        public Injector(Dictionary<Type, object> staticReferences) : base(staticReferences)
        {
            _typeTable = new Dictionary<Type, MemberInfo>();
        }

        internal void Inject(object injectable)
        {
            if (!_typeTable.ContainsKey(injectable.GetType()))
                GetInjects(injectable);
            Inject(injectable, _typeTable[injectable.GetType()]);
        }

        public override void Dispose()
        {
            base.Dispose();
            _typeTable = null;
        }

        private void GetInjects(object injectable)
        {
            var type = injectable.GetType();

            List<FieldInfo> fieldInfos = new List<FieldInfo>();
            List<PropertyInfo> propertyInfos = new List<PropertyInfo>();
            GetFieldsRecursive(type, ref fieldInfos);
            GetPropertiesRecursive(type, ref propertyInfos);

            var memberInfo = new MemberInfo(fieldInfos.Distinct().ToArray(), propertyInfos.Distinct().ToArray());
            _typeTable.Add(type, memberInfo);
        }

        private void GetFieldsRecursive(Type t, ref List<FieldInfo> fields)
        {
            try
            {
                fields.AddRange(t
                    .GetFields(BindingFlags.Instance |
                               BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Public)
                    .Where(fi =>fi.GetCustomAttribute<Inject>() != null || fi.GetCustomAttribute<InjectStatic>() != null)
                    .Select(fi => fi.DeclaringType == null || fi.DeclaringType == t
                        ? fi
                        : fi.DeclaringType.GetField(fi.Name, BindingFlags.Instance |
                                                             BindingFlags.GetField | BindingFlags.NonPublic |
                                                             BindingFlags.Public)));
                if (t.BaseType != null)
                    GetFieldsRecursive(t.BaseType, ref fields);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        private void GetPropertiesRecursive(Type t, ref List<PropertyInfo> propertyInfos)
        {
            try
            {
                propertyInfos.AddRange(t
                    .GetProperties(BindingFlags.Instance | BindingFlags.Static |
                                   BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Public)
                    .Where(pi =>
                        pi.GetCustomAttributes().Any(a =>
                            a.GetType() == typeof(Inject) || a.GetType() == typeof(InjectStatic)))
                    .Select(pi => pi.DeclaringType == null || pi.DeclaringType == t
                        ? pi
                        : pi.DeclaringType.GetProperty(pi.Name, BindingFlags.Instance |
                                                                BindingFlags.GetProperty | BindingFlags.NonPublic |
                                                                BindingFlags.Public)));
            }
            catch (Exception)
            {
                Debug.LogWarning($"Failed to get properties of type {t.FullName}");
            }

            if (t.BaseType != null)
                GetPropertiesRecursive(t.BaseType, ref propertyInfos);
        }

        private void Inject(object injectable, MemberInfo memberInfo)
        {
            InjectField(injectable, memberInfo);
            InjectFieldStatic(injectable, memberInfo);
            InjectProperty(injectable, memberInfo);
            InjectPropertyStatic(injectable, memberInfo);
        }

        private void InjectField(object injectable, MemberInfo memberInfo)
        {
            if (memberInfo.FieldInfos == null)
                return;
            foreach (var fieldInfo in memberInfo.FieldInfos)
                fieldInfo.Key.SetValue(injectable,
                    CreateNewInstance(injectable, fieldInfo.Key.FieldType, fieldInfo.Value));
        }

        private void InjectFieldStatic(object injectable, MemberInfo memberInfo)
        {
            if (memberInfo.StaticFieldInfos == null)
                return;
            foreach (var fieldInfo in memberInfo.StaticFieldInfos)
                fieldInfo.Key.SetValue(injectable, GetStaticReference(fieldInfo.Key.FieldType, fieldInfo.Value));
        }

        private void InjectProperty(object injectable, MemberInfo memberInfo)
        {
            if (memberInfo.PropertyInfos == null)
                return;
            foreach (var propertyInfo in memberInfo.PropertyInfos)
                propertyInfo.Key.SetValue(injectable,
                    CreateNewInstance(injectable, propertyInfo.Key.PropertyType, propertyInfo.Value));
        }

        private void InjectPropertyStatic(object injectable, MemberInfo memberInfo)
        {
            if (memberInfo.StaticPropertyInfos == null)
                return;
            foreach (var propertyInfo in memberInfo.StaticPropertyInfos)
                propertyInfo.Key.SetValue(injectable,
                    GetStaticReference(propertyInfo.Key.PropertyType, propertyInfo.Value));
        }
    }
}