using System;
using System.Collections.Generic;
using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Injectors
{
    internal abstract class Unijector : IDisposable
    {
        protected Dictionary<Type, object> StaticReferences;

        internal Unijector(Dictionary<Type, object> staticReferences)
        {
            StaticReferences = staticReferences;
        }

        protected object GetStaticReference(Type type, Resolver resolver)
        {
            object CreateAndAddStaticInstance()
            {
                var newInstance = CreateNewInstance(null, type, resolver);
                if (newInstance != null)
                    StaticReferences.Add(type, newInstance);
                return newInstance;
            }

            return StaticReferences.TryGetValue(type, out var instance)
                ? instance
                : CreateAndAddStaticInstance();
        }

        protected object CreateNewInstance(object reference, Type t, Resolver resolver)
        {
            if (resolver != null)
            {
                if (resolver.GetType().IsGenericTypeSubClassOrDerivableFrom(typeof(AbstractComponentResolver<>)) &&
                    reference is Component component)
                {
                    var generic = typeof(AbstractComponentResolver<>);
                    Type[] typeArgs = {t};
                    Type constructed = generic.MakeGenericType(typeArgs);
                    dynamic converted = Convert.ChangeType(resolver, constructed);
                    converted.SetComponent(component);


                    if (typeof(List<>)==t || typeof(List<>).IsAssignableFrom(t) || t.IsGenericType)
                        throw new NotImplementedException($"Cannot inject into List<T> collection type! For {component.name} on property/field with type {t.FullName}");
                    else
                        return converted.Resolve();
                }
                else if (resolver is IResolver ir)
                    return ir.Resolve();
            }
            else if (t.GetConstructor(Type.EmptyTypes) != null)
                return Activator.CreateInstance(t);
            else
                Debug.LogWarning(
                    $"No DefaultConstructor found for type {t.FullName}. Please add a DefaultConstructor or Inject with a Resolver!");

            return null;
        }

        public virtual void Dispose()
        {
            foreach (KeyValuePair<Type, object> instance in StaticReferences)
                (instance.Value as IDisposable)?.Dispose();
            StaticReferences = null;
        }
    }
}