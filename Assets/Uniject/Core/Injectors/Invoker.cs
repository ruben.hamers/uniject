using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MethodInfo = HamerSoft.Uniject.Core.Info.MethodInfo;

namespace HamerSoft.Uniject.Core.Injectors
{
    internal class Invoker : Unijector
    {
        private Dictionary<Type, MethodInfo> _methodInfos;

        internal Invoker(Dictionary<Type, object> staticReferences) : base(staticReferences)
        {
            _methodInfos = new Dictionary<Type, MethodInfo>();
        }

        internal void Invoke<T>(object reference, Action<T> method)
            where T : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return;

            if (isStatic)
                method.Invoke(AddOrGetStaticReference<T>());
            else
                method.Invoke(new T());
        }

        internal void Invoke<T1, T2>(object reference, Action<T1, T2> method)
            where T1 : new()
            where T2 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return;

            if (isStatic)
                method.Invoke(AddOrGetStaticReference<T1>(), AddOrGetStaticReference<T2>());
            else
                method.Invoke(new T1(), new T2());
        }

        internal void Invoke<T1, T2, T3>(object reference, Action<T1, T2, T3> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return;

            if (isStatic)
                method.Invoke(
                    AddOrGetStaticReference<T1>(),
                    AddOrGetStaticReference<T2>(),
                    AddOrGetStaticReference<T3>());
            else
                method.Invoke(new T1(), new T2(), new T3());
        }

        internal void Invoke<T1, T2, T3, T4>(object reference, Action<T1, T2, T3, T4> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
            where T4 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return;

            if (isStatic)
                method.Invoke(
                    AddOrGetStaticReference<T1>(),
                    AddOrGetStaticReference<T2>(),
                    AddOrGetStaticReference<T3>(),
                    AddOrGetStaticReference<T4>());
            else
                method.Invoke(new T1(), new T2(), new T3(), new T4());
        }

        internal TReturn Invoke<T, TReturn>(object reference, Func<T, TReturn> method) where T : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return default;

            if (isStatic)
                return method.Invoke(AddOrGetStaticReference<T>());
            else
                return method.Invoke(new T());
        }

        internal TReturn Invoke<T1, T2, TReturn>(object reference, Func<T1, T2, TReturn> method)
            where T1 : new()
            where T2 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return default;

            if (isStatic)
                return method.Invoke(
                    AddOrGetStaticReference<T1>(),
                    AddOrGetStaticReference<T2>());
            else
                return method.Invoke(new T1(), new T2());
        }

        internal TReturn Invoke<T1, T2, T3, TReturn>(object reference, Func<T1, T2, T3, TReturn> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return default;

            if (isStatic)
                return method.Invoke(
                    AddOrGetStaticReference<T1>(),
                    AddOrGetStaticReference<T2>(),
                    AddOrGetStaticReference<T3>());
            else
                return method.Invoke(new T1(), new T2(), new T3());
        }

        internal TReturn Invoke<T1, T2, T3, T4, TReturn>(object reference, Func<T1, T2, T3, T4, TReturn> method)
            where T1 : new()
            where T2 : new()
            where T3 : new()
            where T4 : new()
        {
            if (!MethodExists(reference, method.Method, out var isStatic))
                return default;

            if (isStatic)
                return method.Invoke(
                    AddOrGetStaticReference<T1>(),
                    AddOrGetStaticReference<T2>(),
                    AddOrGetStaticReference<T3>(),
                    AddOrGetStaticReference<T4>());
            else
                return method.Invoke(new T1(), new T2(), new T3(), new T4());
        }

        private T AddOrGetStaticReference<T>() where T : new()
        {
            if (!StaticReferences.ContainsKey(typeof(T)))
                StaticReferences.Add(typeof(T), new T());
            return (T) StaticReferences[typeof(T)];
        }

        private bool MethodExists(object reference, System.Reflection.MethodInfo method, out bool isStatic)
        {
            Type type = reference.GetType();
            GetMethodInfos(reference);
            isStatic = false;
            if (GetMethodInfo(_methodInfos[type].Methods.Keys.ToArray(), method))
                return true;
            else if (GetMethodInfo(_methodInfos[type].MethodsStatic.Keys.ToArray(), method))
            {
                isStatic = true;
                return true;
            }

            return false;
        }

        private bool GetMethodInfo(System.Reflection.MethodInfo[] infos, System.Reflection.MethodInfo method)
        {
            return infos.FirstOrDefault(mi => mi.Equals(method)) != null;
        }

        private void GetMethodInfos(object reference)
        {
            if (_methodInfos.ContainsKey(reference.GetType()))
                return;
            _methodInfos.Add(reference.GetType(),
                new MethodInfo(reference
                    .GetType()
                    .GetMethods(BindingFlags.Instance |
                                BindingFlags.Public |
                                BindingFlags.Static |
                                BindingFlags.FlattenHierarchy |
                                BindingFlags.NonPublic)));
        }

        public void Dispose()
        {
            _methodInfos = null;
        }
    }
}