﻿using System;

namespace HamerSoft.Uniject.Core.Attributes
{
    public class Inject : AbstractInjector
    {
        public Inject()
        {
        }

        public Inject(Type iResolver) : base(iResolver)
        {
        }
    }
}