using System;
using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Attributes
{
    public abstract class AbstractInjector : Attribute
    {
        private Type _resolver;

        public AbstractInjector()
        {
        }

        public AbstractInjector(Type iResolver)
        {
            if (iResolver.IsGenericTypeSubClassOrDerivableFrom(typeof(IComponentResolver<>)) || iResolver.IsGenericTypeSubClassOrDerivableFrom(typeof(Resolver)))
                _resolver = iResolver;
            else
                Debug.LogWarning(
                    $"Added resolver of type {iResolver.FullName} but it does not inherit from HamerSoft.Uniject.Core.IResolver or HamerSoft.Uniject.Core.IResolver<> or HamerSoft.Uniject.Core.IComponentResolver<>");
        }

        public Type GetResolver()
        {
            return _resolver;
        }
    }
}