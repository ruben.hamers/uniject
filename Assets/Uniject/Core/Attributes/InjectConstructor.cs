using System;

namespace HamerSoft.Uniject.Core.Attributes
{
    public class InjectConstructor : AbstractInjector
    {
        public InjectConstructor(Type iResolver) : base(iResolver)
        {
        }
    }
}