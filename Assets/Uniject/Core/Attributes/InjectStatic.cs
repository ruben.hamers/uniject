using System;

namespace HamerSoft.Uniject.Core.Attributes
{
    public class InjectStatic : AbstractInjector
    {
        public InjectStatic()
        {
        }

        public InjectStatic(Type iResolver) : base(iResolver)
        {
        }
    }
}