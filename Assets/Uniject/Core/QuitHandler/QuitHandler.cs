using System;
using UnityEngine;

namespace HamerSoft.Uniject.Core
{
    /// <summary>
    /// hidden object to catch the unity onapplication quit event to cleanup the event bus
    /// it's a shame Unity does not offer a nice event based solution for this..
    /// </summary>
    internal sealed class QuitHandler : MonoBehaviour
    {
        private Action _quitHandler;

        private void Awake()
        {
            DontDestroyOnLoad(this);
            hideFlags = HideFlags.HideInHierarchy;
        }

        private void OnApplicationQuit()
        {
            _quitHandler?.Invoke();
            if (Application.isPlaying)
                Destroy(gameObject);
            else
                Destroy(gameObject);
        }

        private void SetHandler(Action quitHandler)
        {
            _quitHandler = quitHandler;
        }

        internal static void Spawn(Action applicationQuit)
        {
            var qh = new GameObject("QuitHandler").AddComponent<QuitHandler>();
            qh.SetHandler(applicationQuit);
        }
    }
}