using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;

namespace HamerSoft.Uniject.Core.Info
{
    public class MethodInfo
    {
        public Dictionary<System.Reflection.MethodInfo, IResolver> Methods { get; private set; }
        public Dictionary<System.Reflection.MethodInfo, IResolver> MethodsStatic { get; private set; }

        public MethodInfo(System.Reflection.MethodInfo[] infos)
        {
            Methods = GetInfos<Inject>(infos);
            MethodsStatic = GetInfos<InjectStatic>(infos);
        }

        private Dictionary<System.Reflection.MethodInfo, IResolver> GetInfos<T2>(System.Reflection.MethodInfo[] methods)
            where T2 : AbstractInjector
        {
            MethodInfo methodInfo = this;
            return methods.Distinct().Where(mi => mi.GetCustomAttribute<T2>() != null).ToDictionary(mi => mi,
                res => methodInfo.CreateResolver(((AbstractInjector) res.GetCustomAttribute(typeof(AbstractInjector)))
                    .GetResolver()));
        }
        
        private IResolver CreateResolver(Type resolver)
        {
            if (resolver == null)
                return null;
            else
                return resolver.CreateResolver();
        }
    }
}