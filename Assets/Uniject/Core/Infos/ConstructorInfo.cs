using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;

namespace HamerSoft.Uniject.Core.Info
{
    public class ConstructorInfo
    {
        public Dictionary<System.Reflection.ConstructorInfo, Resolver> Methods { get; private set; }

        public ConstructorInfo(System.Reflection.ConstructorInfo[] infos)
        {
            Methods = GetInfos<InjectConstructor>(infos);
        }

        private Dictionary<System.Reflection.ConstructorInfo, Resolver> GetInfos<T2>(
            System.Reflection.ConstructorInfo[] methods)
            where T2 : AbstractInjector
        {
            ConstructorInfo methodInfo = this;
            return methods.Distinct().Where(mi => mi.GetCustomAttribute<T2>() != null).ToDictionary(mi => mi,
                res => methodInfo.CreateResolver(((AbstractInjector) res.GetCustomAttribute(typeof(AbstractInjector)))
                    .GetResolver()));
        }

        private Resolver CreateResolver(Type resolver)
        {
            if (resolver == null)
                return null;
            else
                return resolver.CreateResolver();
        }
    }
}