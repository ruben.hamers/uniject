using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;

namespace HamerSoft.Uniject.Core.Info
{
    public struct MemberInfo
    {
        public Dictionary<FieldInfo, Resolver> FieldInfos;
        public Dictionary<PropertyInfo, Resolver> PropertyInfos;
        public Dictionary<FieldInfo, Resolver> StaticFieldInfos;
        public Dictionary<PropertyInfo, Resolver> StaticPropertyInfos;

        public MemberInfo(FieldInfo[] fields, PropertyInfo[] properties) : this()
        {
            FieldInfos = GetInfos<Inject>(fields);
            StaticFieldInfos = GetInfos<InjectStatic>(fields);
            PropertyInfos = GetInfos<Inject>(properties);
            StaticPropertyInfos = GetInfos<InjectStatic>(properties);
        }

        private Dictionary<FieldInfo, Resolver> GetInfos<T>(System.Reflection.FieldInfo[] fields)
            where T : AbstractInjector
        {
            MemberInfo memberInfo = this;
            return fields.Distinct().Where(mi => mi.GetCustomAttribute<T>() != null).ToDictionary(fi => fi,
                res => memberInfo.CreateResolver(((AbstractInjector) res.GetCustomAttribute(typeof(AbstractInjector)))
                    .GetResolver(), res.FieldType));
        }

        private Dictionary<PropertyInfo, Resolver> GetInfos<T>(System.Reflection.PropertyInfo[] fields)
            where T : AbstractInjector
        {
            MemberInfo memberInfo = this;
            return fields.Distinct().Where(mi => mi.GetCustomAttribute<T>() != null).ToDictionary(fi => fi,
                res => memberInfo.CreateResolver(((AbstractInjector) res.GetCustomAttribute(typeof(AbstractInjector)))
                    .GetResolver(), res.PropertyType));
        }

        private Resolver CreateResolver(Type resolver, System.Type memberType)
        {
            if (resolver == null)
                return null;
            else if (resolver.IsGenericTypeSubClassOrDerivableFrom(typeof(IComponentResolver<>)))
                return CreateComponentResolver(resolver, memberType);
            else if (resolver.IsGenericTypeSubClassOrDerivableFrom(typeof(IResolver)))
                return CreateIResolver(resolver);
        

            return null;
        }

        private Resolver CreateComponentResolver(Type resolver, System.Type memberType)
        {
            if (resolver == null)
                return null;
            else
                return resolver.CreateComponentResolver(memberType);
        }

        private IResolver CreateIResolver(Type resolver)
        {
            if (resolver == null)
                return null;
            else
                return resolver.CreateResolver();
        }
    }
}