using System;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public class ComponentInChildrenResolver<T> : AbstractComponentResolver<T> where T : Component
    {
        protected Func<Component, bool> Query;

        public override T Resolve()
        {
            return Query != null
                ? Component.GetComponentsInChildren<T>().FirstOrDefault(c => Query(c))
                : Component.GetComponentInChildren<T>();
        }
    }
}