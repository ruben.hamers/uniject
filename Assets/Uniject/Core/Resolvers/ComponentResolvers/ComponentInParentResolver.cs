using System;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public class ComponentInParentResolver<T> : AbstractComponentResolver<T> where T : Component
    {
        protected Func<Component, bool> Query;

        public override T Resolve()
        {
            if (Query != null)
                return Component.GetComponentsInParent<T>().FirstOrDefault(c => Query(c));
            else
                return Component.GetComponentInParent<T>();
        }
    }
}