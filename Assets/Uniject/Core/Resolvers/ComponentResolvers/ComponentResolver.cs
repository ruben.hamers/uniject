using UnityEngine;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public class ComponentResolver<T> : AbstractComponentResolver<T> where T : Component
    {
        public override T Resolve()
        {
            return Component.GetComponent<T>();
        }
    }
}