using System;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public abstract class AbstractComponentResolver<T> : IComponentResolver<T>
    {
        protected Component Component;

        public void SetComponent(Component component)
        {
            Component = component;
        }

        public abstract T Resolve();

        public TypeCode GetTypeCode()
        {
            return TypeCode.Object;
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            return this;
        }

        public string ToString(IFormatProvider provider)
        {
            return GetType()?.FullName ?? "Implementation of ComponentResolver<T>";
        }

        #region Unimplemented IConvertable

        public bool ToBoolean(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public byte ToByte(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public char ToChar(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public double ToDouble(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public short ToInt16(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public int ToInt32(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public long ToInt64(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public float ToSingle(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}