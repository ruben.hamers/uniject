using System;
using System.Linq;
using UnityEngine;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public class ComponentsInChildrenResolver<T> : AbstractComponentResolver<T[]> where T : Component
    {
        protected Func<Component, bool> Query;

        public override T[] Resolve()
        {
            if (Query != null)
                return Component.GetComponentsInChildren<T>().Where(c => Query(c)).ToArray();
            else
                return Component.GetComponentsInChildren<T>().ToArray();
        }
    }
}