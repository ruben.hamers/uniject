using System;

namespace HamerSoft.Uniject.Core.Resolvers
{
    public interface Resolver
    {
    }

    public interface IResolver : Resolver
    {
        object Resolve();
    }

    public interface IResolver<T> : Resolver
    {
        void Resolve(Action<T> callback);
    }

    public interface IComponentResolver<T> : Resolver, IConvertible
    {
        T Resolve();
    }
}