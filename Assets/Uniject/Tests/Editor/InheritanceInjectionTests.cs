using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;

namespace HamerSoft.Uniject.Tests
{
    public class InheritanceInjectionTests : InjectionTest
    {
        [Test]
        public void WhenChildIsInjected_TheParentValuesAreSet()
        {
            var child = new AllOptionsChild(true, false);
            Assert.NotNull(child.publicPrivateServiceProp);
            Assert.NotNull(child.GetPrivateServiceField);
        }

        [Test]
        public void WhenParentIsCallsInject_TheChildPropertiesAreAlsoInjected()
        {
            var child = new AllOptionsChild(false, true);
            Assert.NotNull(child.publicPrivateServiceProp);
            Assert.NotNull(child.GetPrivateServiceField);
            Assert.NotNull(child.internalPrivateServiceProp2);
        }
    }
}