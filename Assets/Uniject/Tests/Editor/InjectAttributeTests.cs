using System.Reflection;
using HamerSoft.Uniject.Core;
using NUnit.Framework;
using HamerSoft.Uniject.Tests.Mocks;

namespace HamerSoft.Uniject.Tests
{
    public class InjectAttributeTests : InjectionTest
    {
        [Test]
        public void InjectAttribute_CanSet_PublicField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicServiceField);
        }

        [Test]
        public void InjectAttribute_CanSet_ProtectedField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetProtectedServiceField);
        }

        [Test]
        public void InjectAttribute_CanSet_InternalField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetInternalServiceField);
        }

        [Test]
        public void InjectAttribute_CanSet_PrivateField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetPrivateServiceField);
        }

        [Test]
        public void InjectAttribute_CanSet_PublicProperty_With_PublicSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicServiceProp);
        }

        [Test]
        public void InjectAttribute_CanSet_PublicProperty_With_ProtectedSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicProtectedServiceProp);
        }

        [Test]
        public void InjectAttribute_CanSet_PublicProperty_With_InternalSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicInternalServiceProp);
        }

        [Test]
        public void InjectAttribute_CanSet_PublicProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicPrivateServiceProp);
        }

        [Test]
        public void InjectAttribute_CanSet_ProtectedProperty_With_ProtectedSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "protectedServiceProp",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectAttribute_CanSet_ProtectedProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "protectedPrivateServiceProp",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectAttribute_CanSet_PrivateProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "privateServiceProp",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectAttribute_CanSet_InternalProperty_With_InternalSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "internalServiceProp",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectAttribute_CanSet_InternalProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "internalPrivateServiceProp",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PublicField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicServiceFieldStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_ProtectedField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetProtectedServiceFieldStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_InternalField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetInternalServiceFieldStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PrivateField()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.GetPrivateServiceFieldStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PublicProperty_With_PublicSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicServicePropStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PublicProperty_With_ProtectedSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicProtectedServicePropStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PublicProperty_With_InternalSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicInternalServicePropStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PublicProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(allOptions.publicPrivateServicePropStatic);
        }

        [Test]
        public void InjectStaticAttribute_CanSet_ProtectedProperty_With_ProtectedSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "protectedServicePropStatic",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectStaticAttribute_CanSet_ProtectedProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "protectedPrivateServicePropStatic",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectStaticAttribute_CanSet_PrivateProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "privateServicePropStatic",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectStaticAttribute_CanSet_InternalProperty_With_InternalSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "internalServicePropStatic",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void InjectStaticAttribute_CanSet_InternalProperty_With_PrivateSet()
        {
            var allOptions = new AllOptions(true);
            Assert.NotNull(GetPropertyObject(allOptions, "internalPrivateServicePropStatic",
                BindingFlags.Instance | BindingFlags.NonPublic));
        }

        [Test]
        public void NoException_IsThrown_WhenResolver_IsNeeded()
        {
            Assert.DoesNotThrow(() =>
            {
                var allOptions = new AllOptions(true);
                Assert.IsNull(allOptions.ServiceWithArgumentsBoolIntResolverless);
            });
        }

        [Test]
        public void When_StaticInject_FailsTo_Instantiate_The_NULLReference_IsNot_CachedForLaterUse()
        {
            Assert.DoesNotThrow(() =>
            {
                var incorrectStaticInject = new IncorrectStaticInject();
                incorrectStaticInject.Inject();
                Assert.Null(incorrectStaticInject.IncorrectInject);
                Assert.NotNull(incorrectStaticInject.CorrectInject);
            });
        }

        private object GetPropertyObject(object target, string name, BindingFlags flags)
        {
            return typeof(AllOptions).GetProperty(name, flags).GetValue(target);
        }
    }
}