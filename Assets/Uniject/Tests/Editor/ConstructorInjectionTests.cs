using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers;
using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;

namespace HamerSoft.Uniject.Tests
{
    public class ConstructorInjectionTests : InjectionTest
    {
        [Test]
        public void InjectOnConstructor_Injects_All_Values()
        {
            var constructorMock = this.Construct<ConstructorInjections, ConstructorObjectResolverIntBool>();
            Assert.NotNull(constructorMock);
            Assert.AreEqual(3, constructorMock.A);
            Assert.AreEqual(true, constructorMock.B);
        }

        [Test]
        public void InjectOnConstructor_WithCallback_Injects_All_Values()
        {
            this.Construct<ConstructorInjections, ConstructorObjectResolver>(constructorMock =>
            {
                Assert.NotNull(constructorMock);
                Assert.AreEqual(3, constructorMock.A);
                Assert.AreEqual(true, constructorMock.B);
            });
        }

        [Test]
        public void NoException_IsThrown_WhenThere_NoMatchingConstructor_IsFound()
        {
            Assert.DoesNotThrow(() =>
            {
                var constructorMock = this.Construct<ConstructorInjections, ServiceWithArgumentsBoolIntResolver>();
                Assert.IsNull(constructorMock);
            });
        }
    }
}