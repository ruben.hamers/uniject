using System.Text;
using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;

namespace HamerSoft.Uniject.Tests
{
    public class MethodInjectionTests : InjectionTest
    {
        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<int>(methodInjections.IntMethod);
            Assert.Greater(methodInjections.A, 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_With_2_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<int, StringBuilder>(methodInjections.IntMethod);
            Assert.Greater(methodInjections.A, 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_With_3_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<int, StringBuilder, bool>(methodInjections.IntMethod);
            Assert.Greater(methodInjections.A, 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_With_4_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<int, StringBuilder, bool, float>(methodInjections.IntMethod);
            Assert.Greater(methodInjections.A, 0);
        }

        [Test]
        public void InjectAttribute_InjectsObjectDependencies_IntoMethods()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<StringBuilder>(methodInjections.StringBuilderMethod);
            Assert.NotNull(methodInjections.Builder);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues()
        {
            var methodInjections = new MethodInjection();
            Assert.Greater(methodInjections.Invoke<int, int>(methodInjections.IntMethodReturn), 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_2_Params()
        {
            var methodInjections = new MethodInjection();
            Assert.Greater(methodInjections.Invoke<int, StringBuilder, int>(methodInjections.IntMethodReturn), 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_3_Params()
        {
            var methodInjections = new MethodInjection();
            Assert.Greater(methodInjections.Invoke<int, StringBuilder, bool, int>(methodInjections.IntMethodReturn), 0);
        }

        [Test]
        public void InjectAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_4_Params()
        {
            var methodInjections = new MethodInjection();
            Assert.Greater(
                methodInjections.Invoke<int, StringBuilder, bool, float, int>(methodInjections.IntMethodReturn), 0);
        }

        [Test]
        public void InjectAttribute_InjectsObjectDependencies_IntoMethods_WithReturnValues()
        {
            var methodInjections = new MethodInjection();
            Assert.NotNull(
                methodInjections.Invoke<StringBuilder, string>(methodInjections.StringBuilderMethodReturnString));
        }

        [Test]
        public void InjectStaticAttribute_InjectsObjectDependencies_IntoMethods()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<TestService>(methodInjections.ServiceMethod);
            Assert.NotNull(methodInjections.Service);
        }

        [Test]
        public void InjectStaticAttribute_InjectsObjectDependencies_IntoMethods_With_2_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<TestService, int>(methodInjections.ServiceMethod);
            Assert.NotNull(methodInjections.Service);
        }

        [Test]
        public void InjectStaticAttribute_InjectsObjectDependencies_IntoMethods_With_3_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<TestService, int, bool>(methodInjections.ServiceMethod);
            Assert.NotNull(methodInjections.Service);
        }

        [Test]
        public void InjectStaticAttribute_InjectsObjectDependencies_IntoMethods_With_4_Params()
        {
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<TestService, int, bool, StringBuilder>(methodInjections.ServiceMethod);
            Assert.NotNull(methodInjections.Service);
        }

        [Test]
        public void InjectStaticAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues()
        {
            var allOptions = new AllOptions(true);
            var methodInjections = new MethodInjection();
            Assert.AreEqual(allOptions.internalServiceFieldStatic.Id,
                methodInjections.Invoke<TestService, TestService>(methodInjections.ServiceMethodReturn).Id);
        }

        [Test]
        public void InjectStaticAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_2_Params()
        {
            var allOptions = new AllOptions(true);
            var methodInjections = new MethodInjection();
            Assert.AreEqual(allOptions.internalServiceFieldStatic.Id,
                methodInjections.Invoke<TestService, int, TestService>(methodInjections.ServiceMethodReturn).Id);
        }

        [Test]
        public void InjectStaticAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_3_Params()
        {
            var allOptions = new AllOptions(true);
            var methodInjections = new MethodInjection();
            Assert.AreEqual(allOptions.internalServiceFieldStatic.Id,
                methodInjections.Invoke<TestService, int, bool, TestService>(methodInjections.ServiceMethodReturn).Id);
        }

        [Test]
        public void InjectStaticAttribute_InjectsValueDependencies_IntoMethods_WithReturnValues_With_4_Params()
        {
            var allOptions = new AllOptions(true);
            var methodInjections = new MethodInjection();
            Assert.AreEqual(allOptions.internalServiceFieldStatic.Id,
                methodInjections
                    .Invoke<TestService, int, bool, StringBuilder, TestService>(methodInjections.ServiceMethodReturn)
                    .Id);
        }
    }
}