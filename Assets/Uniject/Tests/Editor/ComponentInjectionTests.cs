using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;
using UnityEngine;

namespace HamerSoft.Uniject.Tests
{
    public class ComponentInjectionTests : InjectionTest
    {
        private MainComponent _main;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _main = new GameObject().AddComponent<MainComponent>();
            _main.Awake();
        }

        [Test]
        public void InjectWithComponentResolver_Injects_TheAttachedComponent()
        {
            Assert.NotNull(_main.SelfComponent);
        }
        
        [Test]
        public void InjectWithComponentResolver_Injects_TheChildComponent()
        {
            Assert.NotNull(_main.Child0Component);
        }
        
        [Test]
        public void InjectWithComponentResolver_Injects_TheChildrenComponents()
        {
            Assert.NotNull(_main.ChildrenComponents);
        }
        
        [Test]
        public void InjectWithComponentResolver_WithFilter_Injects_TheChild1Component()
        {
            Assert.NotNull(_main.Child1Component);
        }
        
        [Test]
        public void InjectWithComponentResolver_WithFilter_Injects_TheChildren1Components()
        {
            Assert.NotNull(_main.ChildrenContaining1Components);
            Assert.AreEqual(2,_main.ChildrenContaining1Components.Length);
        }
        [Test]
        public void InjectWithComponentResolver_Injects_ParentComponent()
        {
            Assert.NotNull(_main.ParentComponents);
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            Object.DestroyImmediate(_main.gameObject);
        }
    }
}