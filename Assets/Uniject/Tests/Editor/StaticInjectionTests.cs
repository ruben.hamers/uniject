using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;

namespace HamerSoft.Uniject.Tests
{
    public class StaticInjectionTests : InjectionTest
    {
        [Test]
        public void InjectStatic_InjectsObjects_WithIdenticalReference()
        {
            var allOptions = new AllOptions(true);
            var allOptions2 = new AllOptions(true);

            Assert.AreEqual(allOptions.publicServiceFieldStatic.Id, allOptions2.publicServiceFieldStatic.Id);
        }

        [Test]
        public void DifferentInjects_OfTheSameType_DoNotShare_Reference()
        {
            var allOptions = new AllOptions(true);
            Assert.AreNotEqual(allOptions.GetProtectedServiceField, allOptions.publicServiceField);
        }

        [Test]
        public void InjectStatic_InjectsObjects_That_Do_Not_Share_Reference_With_Inject()
        {
            var allOptions = new AllOptions(true);

            Assert.AreNotEqual(allOptions.publicServiceField.Id, allOptions.publicServiceFieldStatic.Id);
        }
        
        [Test]
        public void InjectStatic_InjectsObjects_ShareReference_With_FieldsAndMethods()
        {
            var allOptions = new AllOptions(true);
            var methodInjections = new MethodInjection();
            methodInjections.Invoke<TestService>(methodInjections.ServiceMethod);
            Assert.AreEqual(methodInjections.Service.Id, allOptions.publicServiceFieldStatic.Id);
        }
    }
}