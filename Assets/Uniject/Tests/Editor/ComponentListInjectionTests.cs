using System;
using HamerSoft.Uniject.Tests.Mocks;
using NUnit.Framework;
using UnityEngine;
using Object = UnityEngine.Object;

namespace HamerSoft.Uniject.Tests
{
    public class ComponentListInjectionTests : InjectionTest
    {
        private MainComponentWithList _main;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            _main = new GameObject().AddComponent<MainComponentWithList>();
        }

        [Test]
        public void InjectWithComponentResolver_WithFilter_Injects_ThrowsExceptionWhen_Field_Is_List()
        {
            Assert.Throws<NotImplementedException>(() => { _main.Awake(); });
        }

        [TearDown]
        public override void TearDown()
        {
            base.TearDown();
            Object.DestroyImmediate(_main.gameObject);
        }
    }
}