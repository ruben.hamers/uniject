using System.Text;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class ConstructorInjections
    {
        public readonly StringBuilder Builder;
        public readonly TestService Service;
        public readonly int A;
        public readonly bool B;

        [InjectConstructor(typeof(ConstructorObjectResolverIntBool))]
        public ConstructorInjections(int a, bool b)
        {
            A = a;
            B = b;
        }

        [InjectConstructor(typeof(ConstructorObjectResolverStringBuilder))]
        public ConstructorInjections(StringBuilder builder)
        {
            Builder = builder;
        }

        [InjectConstructor(typeof(ConstructorObjectResolver))]
        public ConstructorInjections(int a, bool b, StringBuilder builder)
        {
            A = a;
            B = b;
            Builder = builder;
        }
    }
}