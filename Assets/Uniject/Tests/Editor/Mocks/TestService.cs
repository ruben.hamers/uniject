namespace HamerSoft.Uniject.Tests.Mocks
{
    public class TestService
    {
        public string Id { get; private set; }

        public TestService()
        {
            Id = System.Guid.NewGuid().ToString(); //+ new Random().Next(0,10000) + DateTime.Now.Millisecond;
        }
    }
}