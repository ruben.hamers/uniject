using System;
using System.Text;
using HamerSoft.Uniject.Core.Resolvers;
using HamerSoft.Uniject.Tests.Mocks;

namespace HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers
{
    public class ConstructorObjectResolverIntBool : IResolver
    {
        public object Resolve()
        {
            return new ConstructorInjections(3, true);
        }
    }

    public class ConstructorObjectResolverStringBuilder : IResolver
    {
        public object Resolve()
        {
            return new ConstructorInjections(new StringBuilder());
        }
    }
    public class ConstructorObjectResolver : IResolver<ConstructorInjections>
    {
        public void Resolve(Action<ConstructorInjections> callback)
        {
            //do some HTTP request and invoke the callback on response.
            callback.Invoke(new ConstructorInjections(3, true, new StringBuilder()));
        }
    }
}