using System.Text;
using HamerSoft.Uniject.Core.Resolvers;
using HamerSoft.Uniject.Tests.Mocks;

namespace HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers
{
    public class ServiceWithArgumentsBoolIntResolver : IResolver
    {
        public ServiceWithArgumentsBoolIntResolver(){}
        public object Resolve()
        {
            return new ServiceWithConstructorArguments(new StringBuilder());
        }
    }
}