using System.Text;
using HamerSoft.Uniject.Core.Resolvers;
using HamerSoft.Uniject.Tests.Mocks;

namespace HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers
{
    public class ServiceWithArgumentsStringBuilderResolver : IResolver
    {
        public ServiceWithArgumentsStringBuilderResolver()
        {
        }

        public object Resolve()
        {
            return new ServiceWithConstructorArguments(new StringBuilder());
        }
    }
}