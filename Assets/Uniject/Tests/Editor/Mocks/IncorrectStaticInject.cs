using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class IncorrectStaticInject
    {
        // no resolver, yet it is needed!
        [InjectStatic]
        public ServiceWithConstructorArguments IncorrectInject { get; set; }

        [InjectStatic(typeof(ServiceWithArgumentsStringBuilderResolver))]
        public ServiceWithConstructorArguments CorrectInject { get; set; }
    }
}