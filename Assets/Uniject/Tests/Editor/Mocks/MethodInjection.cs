using System.Text;
using HamerSoft.Uniject.Core.Attributes;
using UnityEngine;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class MethodInjection
    {
        public int A;
        public StringBuilder Builder;
        public TestService Service;

        [Inject]
        public void IntMethod(int a)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
        }

        [Inject]
        public void IntMethod(int a, StringBuilder s)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
        }

        [Inject]
        public void IntMethod(int a, StringBuilder s, bool b)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
        }

        [Inject]
        public void IntMethod(int a, StringBuilder s, bool b, float f)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
        }

        [Inject]
        public int IntMethodReturn(int a)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
            return A;
        }

        [Inject]
        public int IntMethodReturn(int a, StringBuilder s)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
            return A;
        }

        [Inject]
        public int IntMethodReturn(int a, StringBuilder s, bool b)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
            return A;
        }

        [Inject]
        public int IntMethodReturn(int a, StringBuilder s, bool b, float f)
        {
            a++;
            Debug.Log($"a = {a}");
            A = a;
            return A;
        }

        [Inject]
        public void StringBuilderMethod(StringBuilder builder)
        {
            builder.Append("oh yes op yes");
            Debug.Log($"builder = {builder}");
            Builder = builder;
        }

        [InjectStatic]
        public string StringBuilderMethodReturnString(StringBuilder builder)
        {
            string result = $"builder = {builder}";
            Debug.Log(result);
            return result;
        }

        [InjectStatic]
        public void ServiceMethod(TestService service)
        {
            Debug.Log($"service = {service.Id}");
            Service = service;
        }

        [InjectStatic]
        public void ServiceMethod(TestService service, int a)
        {
            Debug.Log($"service = {service.Id}");
            Service = service;
        }

        [InjectStatic]
        public void ServiceMethod(TestService service, int a, bool b)
        {
            Debug.Log($"service = {service.Id}");
            Service = service;
        }

        [InjectStatic]
        public void ServiceMethod(TestService service, int a, bool b, StringBuilder s)
        {
            Debug.Log($"service = {service.Id}");
            Service = service;
        }

        [InjectStatic]
        public TestService ServiceMethodReturn(TestService service)
        {
            Debug.Log($"service = {service.Id}");
            return service;
        }

        [InjectStatic]
        public TestService ServiceMethodReturn(TestService service, int a)
        {
            Debug.Log($"service = {service.Id}");
            return service;
        }

        [InjectStatic]
        public TestService ServiceMethodReturn(TestService service, int a, bool b)
        {
            Debug.Log($"service = {service.Id}");
            return service;
        }

        [InjectStatic]
        public TestService ServiceMethodReturn(TestService service, int a, bool b, StringBuilder s)
        {
            Debug.Log($"service = {service.Id}");
            return service;
        }
    }
}