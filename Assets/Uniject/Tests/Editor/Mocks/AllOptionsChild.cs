using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Core.Attributes;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class AllOptionsChild : AllOptions
    {
        [Inject] protected TestService protectedServiceField2;
        [Inject] private TestService privateServiceField;
        [Inject] public TestService publicServiceField2;
        [Inject] internal TestService internalServiceField2;
        
        [Inject] protected TestService protectedServiceProp2 { get; set; }
        [Inject] protected TestService protectedPrivateServiceProp2 { get; private set; }
        [Inject] private TestService privateServiceProp { get; set; }
        [Inject] public TestService publicServiceProp2 { get; set; }
        [Inject] public TestService publicInternalServiceProp2 { get; internal set; }
        [Inject] public TestService publicProtectedServiceProp2 { get; protected set; }
        [Inject] public TestService publicPrivateServiceProp2 { get; private set; }
        [Inject] internal TestService internalServiceProp2 { get; set; }
        [Inject] internal TestService internalPrivateServiceProp2 { get; private set; }
        
        [InjectStatic] protected TestService protectedServiceFieldStatic2;
        [InjectStatic] private TestService privateServiceFieldStatic;
        [InjectStatic] public TestService publicServiceFieldStatic2;
        [InjectStatic] internal TestService internalServiceFieldStatic2;
        
        [InjectStatic] protected TestService protectedServicePropStatic2 { get; set; }
        [InjectStatic] protected TestService protectedPrivateServicePropStatic2 { get; private set; }
        [InjectStatic] private TestService privateServicePropStatic { get; set; }
        [InjectStatic] public TestService publicServicePropStatic2 { get; set; }
        [InjectStatic] public TestService publicInternalServicePropStatic2 { get; internal set; }
        [InjectStatic] public TestService publicProtectedServicePropStatic2 { get; protected set; }
        [InjectStatic] public TestService publicPrivateServicePropStatic2 { get; private set; }
        [InjectStatic] internal TestService internalServicePropStatic2 { get; set; }
        [InjectStatic] internal TestService internalPrivateServicePropStatic2 { get; private set; }

        public AllOptionsChild(bool injectSelf, bool injectParent) : base(injectParent)
        {
            if (injectSelf)
                this.Inject();
        }
    }
}