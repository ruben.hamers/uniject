using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Tests.Editor.Mocks.Resolvers;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class AllOptions
    {
        // instanced injects
        [Inject] protected TestService protectedServiceField;
        [Inject] private TestService privateServiceField;
        [Inject] public TestService publicServiceField;
        [Inject] internal TestService internalServiceField;
        public TestService GetProtectedServiceField => protectedServiceField;
        public TestService GetPrivateServiceField => privateServiceField;
        public TestService GetInternalServiceField => internalServiceField;

        [Inject] protected TestService protectedServiceProp { get; set; }
        [Inject] protected TestService protectedPrivateServiceProp { get; private set; }
        [Inject] private TestService privateServiceProp { get; set; }
        [Inject] public TestService publicServiceProp { get; set; }
        [Inject] public TestService publicInternalServiceProp { get; internal set; }
        [Inject] public TestService publicProtectedServiceProp { get; protected set; }
        [Inject] public TestService publicPrivateServiceProp { get; private set; }
        [Inject] internal TestService internalServiceProp { get; set; }
        [Inject] internal TestService internalPrivateServiceProp { get; private set; }

        //static injects
        [InjectStatic] protected TestService protectedServiceFieldStatic;
        [InjectStatic] private TestService privateServiceFieldStatic;
        [InjectStatic] public TestService publicServiceFieldStatic;
        [InjectStatic] internal TestService internalServiceFieldStatic;

        public TestService GetProtectedServiceFieldStatic => protectedServiceFieldStatic;
        public TestService GetPrivateServiceFieldStatic => privateServiceFieldStatic;
        public TestService GetInternalServiceFieldStatic => internalServiceFieldStatic;

        [InjectStatic] protected TestService protectedServicePropStatic { get; set; }
        [InjectStatic] protected TestService protectedPrivateServicePropStatic { get; private set; }
        [InjectStatic] private TestService privateServicePropStatic { get; set; }
        [InjectStatic] public TestService publicServicePropStatic { get; set; }
        [InjectStatic] public TestService publicInternalServicePropStatic { get; internal set; }
        [InjectStatic] public TestService publicProtectedServicePropStatic { get; protected set; }
        [InjectStatic] public TestService publicPrivateServicePropStatic { get; private set; }
        [InjectStatic] internal TestService internalServicePropStatic { get; set; }
        [InjectStatic] internal TestService internalPrivateServicePropStatic { get; private set; }

        //injects with resolver
        [Inject(typeof(ServiceWithArgumentsBoolIntResolver))]
        public ServiceWithConstructorArguments ServiceWithArgumentsBoolInt { get; set; }

        [Inject(typeof(ServiceWithArgumentsStringBuilderResolver))]
        public ServiceWithConstructorArguments ServiceWithArgumentsStringBuilder { get; set; }

        //statics
        [InjectStatic(typeof(ServiceWithArgumentsBoolIntResolver))]
        public ServiceWithConstructorArguments ServiceWithArgumentsBoolIntStatic { get; set; }

        [InjectStatic(typeof(ServiceWithArgumentsStringBuilderResolver))]
        public ServiceWithConstructorArguments ServiceWithArgumentsStringBuilderStatic { get; set; }

        // resolver actually needed, yet forgotten. The following property will be null
        [Inject] public ServiceWithConstructorArguments ServiceWithArgumentsBoolIntResolverless { get; set; }

        public AllOptions(bool inject)
        {
            if (inject)
                this.Inject();
        }
    }
}