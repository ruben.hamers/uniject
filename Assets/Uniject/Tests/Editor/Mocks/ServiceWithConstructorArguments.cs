using System.Text;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class ServiceWithConstructorArguments
    {
        public readonly StringBuilder SB;
        public readonly bool B;
        public readonly int I;

        public ServiceWithConstructorArguments(bool b, int i)
        {
            B = b;
            I = i;
        }

        public ServiceWithConstructorArguments(StringBuilder sb)
        {
            SB = sb;
        }
    }
}