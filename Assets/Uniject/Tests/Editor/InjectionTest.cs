using HamerSoft.Uniject.Core;
using NUnit.Framework;

namespace HamerSoft.Uniject.Tests
{
    public abstract class InjectionTest
    {
        [SetUp]
        public virtual void Setup()
        {
            InjectExtension.Reset();
        }

        [TearDown]
        public virtual void TearDown()
        {
        }
    }
}