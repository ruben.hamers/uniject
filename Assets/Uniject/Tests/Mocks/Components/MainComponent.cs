using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;
using HamerSoft.Uniject.Tests.Mocks.ComponentResolvers;
using HamerSoft.Uniject.Tests.Mocks.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class MainComponent : MonoBehaviour
    {
        [Inject(typeof(ComponentResolver<SearchableComponent>))]
        public SearchableComponent SelfComponent;

        [Inject(typeof(ComponentInChildrenResolver<SearchableComponent>))]
        public SearchableComponent Child0Component;

        [Inject(typeof(ComponentsInChildrenResolver<SearchableComponent>))]
        public SearchableComponent[] ChildrenComponents;

        [Inject(typeof(Child1Resolver<SearchableComponent>))]
        public SearchableComponent Child1Component;

        [Inject(typeof(Children1Resolver<SearchableComponent>))]
        public SearchableComponent[] ChildrenContaining1Components;

        [Inject(typeof(ComponentsInParentResolver<SearchableComponent>))]
        public SearchableComponent[] ParentComponents;

        public void Awake()
        {
            var parent = SpawnParent();
            gameObject.AddComponent<SearchableComponent>().SetId("self");
            SpawnComponents(transform, "child", 11);
            SpawnComponents(parent, "sibling", 11);
            var grandparent = new GameObject();
            parent.transform.SetParent(grandparent.transform);
            SpawnComponents(grandparent.transform, "grand", 11);
            this.Inject();
        }

        private Transform SpawnParent()
        {
            var parent = new GameObject();
            parent.AddComponent<SearchableComponent>().SetId("Parent");
            transform.SetParent(parent.transform);
            return parent.transform;
        }

        private void SpawnComponents(Transform parent, string idPrefix, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                var child = new GameObject().AddComponent<SearchableComponent>();
                child.SetId($"{idPrefix}{i}");
                child.name = $"{idPrefix}{i}";
                child.transform.SetParent(parent);
            }
        }
    }
}