using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Tests.Mocks.ComponentResolvers
{
    public class Child1Resolver<T> : ComponentInChildrenResolver<T> where T : Component
    {
        public Child1Resolver()
        {
            Query = component => component.name == "child1";
        }
    }
}