using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Tests.Mocks.ComponentResolvers
{
    public class Children1Resolver<T> : ComponentsInChildrenResolver<T> where T : Component
    {
        public Children1Resolver()
        {
            Query = component => component.name.Contains("1");
        }
    }
}