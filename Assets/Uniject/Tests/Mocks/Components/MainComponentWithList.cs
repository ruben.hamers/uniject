using System.Collections.Generic;
using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Tests.Mocks.ComponentResolvers;
using HamerSoft.Uniject.Tests.Mocks.Resolvers;
using UnityEngine;

namespace HamerSoft.Uniject.Tests.Mocks
{
    public class MainComponentWithList : MonoBehaviour
    {
        [Inject(typeof(Children1Resolver<SearchableComponent>))]
        public List<SearchableComponent> ChildrenContaining1Components;

        public void Awake()
        {
            var parent = SpawnParent();
            gameObject.AddComponent<SearchableComponent>().SetId("self");
            SpawnComponents(transform, "child", 12);
            SpawnComponents(parent, "sibling", 12);
            var grandparent = new GameObject();
            parent.transform.SetParent(grandparent.transform);
            SpawnComponents(grandparent.transform, "grand", 12);
            this.Inject();
        }

        private Transform SpawnParent()
        {
            var parent = new GameObject();
            parent.AddComponent<SearchableComponent>().SetId("Parent");
            transform.SetParent(parent.transform);
            return parent.transform;
        }

        private void SpawnComponents(Transform parent, string idPrefix, int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                var child = new GameObject().AddComponent<SearchableComponent>();
                child.SetId($"{idPrefix}{i}");
                child.name = $"{idPrefix}{i}";
                child.transform.SetParent(parent);
            }
        }
    }
}