using System.Security.Cryptography.X509Certificates;
using UnityEngine;
namespace HamerSoft.Uniject.Tests.Mocks.Resolvers
{
    public class SearchableComponent : MonoBehaviour
    {
        public string Id { get; private set; }

        public void SetId(string id)
        {
            this.Id = id;
        }
    }
}