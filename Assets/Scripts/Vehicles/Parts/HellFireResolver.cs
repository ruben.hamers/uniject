using HamerSoft.Uniject.Core.Resolvers;

namespace Readme
{
    public class HellFireResolver : ComponentsInChildrenResolver<WeaponSystem>
    {
        public HellFireResolver()
        {
            Query = component => component is Hellfire;
        }
    }
}