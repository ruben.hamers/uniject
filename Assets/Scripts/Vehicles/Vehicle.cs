using HamerSoft.Uniject.Core;
using UnityEngine;

namespace Readme
{
    public abstract class Vehicle : MonoBehaviour
    {

        protected virtual void Awake()
        {
            this.Inject();
        }
    }
}