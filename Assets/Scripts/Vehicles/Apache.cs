using System;
using HamerSoft.Uniject.Core.Attributes;
using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace Readme
{
    public class Apache : Vehicle
    {
        /// <summary>
        /// Inject with GetComponentsInChildren to get all rotors
        /// </summary>
        [Inject(typeof(ComponentsInChildrenResolver<Rotor>))]
        private Rotor[] _rotors;

        /// <summary>
        /// Inject with GetComponentInChildren type to get the landing gear
        /// </summary>
        [Inject(typeof(ComponentInChildrenResolver<LandingGear>))]
        private LandingGear _landingGear;

        /// <summary>
        /// Inject with GetComponentsInChildren type to get all weaponsystems
        /// </summary>
        [Inject(typeof(ComponentsInChildrenResolver<WeaponSystem>))]
        private WeaponSystem[] _weaponSystems;

        /// <summary>
        /// inject using a special resolver type
        /// </summary>
        [Inject(typeof(HellFireResolver))] private WeaponSystem _activeWeaponSystems;

        /// <summary>
        /// Inject with GetComponent to get the attached audiosource
        /// </summary>
        [Inject(typeof(ComponentResolver<AudioSource>))]
        private AudioSource _audioSource;
    }
}