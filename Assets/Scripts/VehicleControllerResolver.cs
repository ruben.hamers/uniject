using HamerSoft.Uniject.Core.Resolvers;
using UnityEngine;

namespace Readme
{
    public class VehicleControllerResolver : IResolver
    {
        public object Resolve()
        {
            var vehicles = Resources.LoadAll<Vehicle>("Vehicles");
            return new VehicleController(vehicles);
        }
    }
}