using HamerSoft.Uniject.Core;
using HamerSoft.Uniject.Core.Attributes;
using UnityEngine;

namespace Readme
{
    public class Player : MonoBehaviour
    {
        /// <summary>
        /// Inject a weapon object 
        /// </summary>
        [Inject]
        private Weapon _weapon;
        
        /// <summary>
        /// InjectStatic the controller (i.e. singleton), this will be a shared reference among everyone that injects it statically 
        /// </summary>
        [InjectStatic]
        private PlayerController _playerController;
        
        /// <summary>
        /// InjectStatic the VehicleController with a special resolver.
        /// </summary>
        [InjectStatic(typeof(VehicleControllerResolver))]
        private VehicleController _vehicleController;

        /// <summary>
        /// I want a chopper ;)
        /// </summary>
        public Vehicle Chopper { get; private set; }
        
        protected void Awake()
        {
            // Inject this object.
            this.Inject();
            _playerController.RegisterPlayer(this);
            Chopper = GetToThaChoppah();
        }

        private Apache GetToThaChoppah()
        {
            return _vehicleController.GetNewVehicle<Apache>();
        }

        protected virtual void Fire()
        {
            _weapon.PewPew();
        }
    }
}