# Rationale
Uniject started as an experiment of how a Dependency Injection (DI) framework should work in Unity3D.
I did some research and found several frameworks including of course; [Zenject](https://github.com/modesttree/Zenject).
Reading through Zenject's massive README file I wondered why it should be this complex. Yeah I know, Zenject is a large project that does way more than simply injecting dependencies.
It also supports mocking during testing for example. Also, a thing I often scratch my hear for is that DI frameworks depend heavily upon these builder patterns for injecting dependencies.
This is a common design in many DI frameworks like Zenject, [Dagger(2)](https://dagger.dev/), [Unity](http://unitycontainer.org/index.html).
The reason for using the builder patterns is that they are compile-time solutions and not runtime/reflection based solutions. The compile-time builders will definitely have greater performance than the runtime solutions.
Also, you get full support of the compiler for DI, which you do not have with the runtime solutions, and support on all target platforms. 
I do <b>not</b> think the builder patterns at compile time is bad design, but to me it just feels like moving the dependencies to another place and accessing them through the builders. 
I think, in many cases, a DI framework should not be used if you can reach the same goal by simple constructor/method injection. That being said, DI should be used in a number of strategic places to inject for example; object factories. 


But let's stop rambling and explain Uniject's approach to DI.
<i>* Also read the lessons learned section to understand why I would <i>not</i> use Uniject in production. I might still prefer Zenject, with the builder patterns over Uniject's solution. However, I have had some fun writing this, practicing reflection (vodoo) magic to get it all working.</i>

## Lessons Learned
* For Unity3D field and property injection make the most sense. Writing this library I did not think someone would gain much benefit from method and constructor injection. I hinted at this in the previous section; I think DI must be used in a few strategic places and I think field and property injection will be enough to achieve this.
* You quickly end up with many resolver types and objects which might be confusing.
* The C# compiler does not allow me to add LINQ queries as arguments for Attributes. This is a mayor bummer since it kind of beats the purpose of the component injections. (Now you need to write an entirely new class that creates the query, you could also just run the query in for example the Awake() of the component.)
* Playing around with C# reflection feels like one big hack. 

# Introduction
Uniject supports all common DI features; Field and Property Injection, Constructor Injection and Method Injection.
Uniject's reflection based solution also uniquely allows me to access protected, internal and private fields/properties/constructors/methods.
## Injection Attributes
All Fields, Properties, Methods and Constructors you want Uniject to inject must be marked with special kinds of attributes.
Uniject offers 3 kinds of attributes;
* [Inject]: Injects an object of the specific type to a Field, property or method.
* [InjectStatic]: Does essentially the same as [Inject], however all statically injected objects will share references. (for example: If you have a Game Controller/manager/singleton you can inject it statically in all objects that need a reference to it.)
* [InjectConstructor]: Special attribute for constructors.

Additionally; Some objects that you would want to inject might not have a default (without parameters) constructor. In this case, you must use objects called `Resolvers` to help uniject with constructing new objects.
All the above mentioned attributes support an overload where you can supply a resolver type. (Examples will follow soon)
Last but not least, there are special resolvers for Unity3D related `GetComponent<T>()`,`GetComponentInParent<T>()`,`GetComponentsInParent<T>()`,`GetComponentInChildren<T>()` and `GetComponentsInChildren<T>()`. These resolvers will allow you to inject components based on type and an optional linq query. (Examples will follow)  

## Field & Property Injection
To inject a field or property you must annotate it with either the [Inject] or the [InjectStatic] attribute.
For example: 
```c#
public class Player : MonoBehaviour
{
    /// <summary>
    /// Inject a weapon object 
    /// </summary>
    [Inject]
    private Weapon _weapon;
    
    /// <summary>
    /// InjectStatic the controller (i.e. singleton), this will be a shared reference among everyone that injects it statically 
    /// </summary>
    [InjectStatic]
    private PlayerController _playerController;
    
    /// <summary>
    /// InjectStatic the VehicleController with a special resolver.
    /// </summary>
    [InjectStatic(typeof(VehicleControllerResolver))]
    private VehicleController _vehicleController;

    /// <summary>
    /// I want a chopper ;)
    /// </summary>
    public Vehicle Chopper { get; private set; }
    
    protected void Awake()
    {
        // Inject this object.
        this.Inject();
        _playerController.RegisterPlayer(this);
        Chopper = GetToThaChoppah();
    }

    private Apache GetToThaChoppah()
    {
        return _vehicleController.GetNewVehicle<Apache>();
    }

    protected virtual void Fire()
    {
        _weapon.PewPew();
    }
}
```
The code above demonstrates how to inject fields and properties with slight different requirements;
<b>First</b>, a `Weapon` is injected. This will be an object private to the player, where only he has a reference to.
<b>Second</b>, a `PlayerController` is injected statically, meaning; each player in the game references the same PlayerController. Note that you could also inject the playerservice statically in another class and they will still share references.
<b>third</b> , a `VehicleController` in injected statically. This also is a singleton type class so we want it statically. Note that this Inject requires a resolver type because it does not have a default constructor.
The snippets below show the code for the `PlayerController`, `VehicleController` and `VehicleControllerResolver`.
```c#
// Player controller
public class PlayerController
{
    public void RegisterPlayer(Player player)
    {
        // register the player
    }
}

//Vehicle Controller
public class VehicleController
{
    protected Vehicle[] _vehicles;
    public VehicleController(Vehicle[] vehicles)
    {
        _vehicles = vehicles;
    }

    public T GetNewVehicle<T>()
    {
        //Instantiate new vehicle!
        return default;
    }
}

// Vehilce controller resolver
public class VehicleControllerResolver : IResolver
{
    public object Resolve()
    {
        var vehicles = Resources.LoadAll<Vehicle>("Vehicles");
        return new VehicleController(vehicles);
    }
}
```
The VehicleController only has a constructor that accepts a `Vehicle[]` object. This is not something Uniject can generate (other than null or an empty array, but that is probably now what we need).
So we need a special resolver object (implements IResolver interface).
In this case, the resolver queries the Resources of the project for all Vehicle Prefabs and injects them into the service so they can be Instantiated when needed.
### Component Resolvers
In case of MonoBehaviours we often want to use specific components lower down in the object/prefab hierarchy. To Inject those you can use the [Inject] attribute with specific ComponentResolver types:
For component resolvers, the [StaticInject] attributes will not work since it makes no sense to inject components statically. If you need a static/singleton reference you should use plain old c# class objects instead of MonoBehaviours.

|Type|Function|
|----|----|
|ComponentResolver| `GetComponent<T>()` |
|ComponentInParentResolver| `GetComponentInParent<T>()` |
|ComponentsInParentResolver| `GetComponentsInParent<T>()` |
|ComponentInChildrenResolver| `GetComponentInChildren<T>()` |
|ComponentsInChildrenResolver| `GetComponentsInChildren<T>()` |

See the example below for how to use them (not all resolver types are used).
```c#
public class Apache : Vehicle
{
    /// <summary>
    /// Inject with GetComponentsInChildren to get all rotors
    /// </summary>
    [Inject(typeof(ComponentsInChildrenResolver<Rotor>))]
    private Rotor[] _rotors;

    /// <summary>
    /// Inject with GetComponentInChildren type to get the landing gear
    /// </summary>
    [Inject(typeof(ComponentInChildrenResolver<LandingGear>))]
    private LandingGear _landingGear;
    
    /// <summary>
    /// Inject with GetComponentsInChildren type to get all weaponsystems
    /// </summary>
    [Inject(typeof(ComponentsInChildrenResolver<WeaponSystem>))]
    private WeaponSystem[] _weaponSystems;

    /// <summary>
    /// inject using a special resolver type (this is the default active weaponsystem)
    /// </summary>
    [Inject(typeof(HellFireResolver))]
    private WeaponSystem _activeWeaponSystems;

    /// <summary>
    /// Inject with GetComponent to get the attached audiosource
    /// </summary>
    [Inject(typeof(ComponentResolver<AudioSource>))]
    private AudioSource _audioSource;
}

public class HellFireResolver : ComponentsInChildrenResolver<WeaponSystem>
{
    public HellFireResolver()
    {
        Query = component => component is Hellfire;
    }
}
```
So, the Apache Class will Inject all components when it is injected. The resolvers will make sure that the correct queries are executed. In case of the `HellFireResolver` a special query is needed. This Query is set in the constructor of the class.
<i>* We need a special class to add the query to the resolver since the C# compiler only allows compile-time constants to be added as arguments for attribute classes. This is the cleanest workaround I could find but to me it still is not concise enough. Currently I do not really find it easier than simply writing the same query in the Awake for example.</i>
### More Examples:
For more examples on Field and Property injection with or without components please refer to:
[All Inject Options](Assets/Uniject/Tests/Editor/Mocks/AllOptions.cs),
[Component injections](Assets/Uniject/Tests/Mocks/Components/MainComponent.cs),
[Tests](Assets/Uniject/Tests).
## Constructor Injection
Sometimes you would want to create an object but you might not have the correct references to the objects needed for construction. In this case you can use constructor injection and allow Uniject to resolve the correct references.
Constructor injection works with a special kind of attribute; [InjectConstructor]. This attribute requires you to add a resolver type to handle the construction of the object.
For constructor injection I also added a special type of resolver that allows you to construct the object in a callback based manner, for example: image you want to construct an object based on the results of an async http request.
You can implement a resolver that handles this and construct the object accordingly.

The following code shows some examples of how to use Constructor injection.
```c#
public class ConstructorInjections
{
    public readonly StringBuilder Builder;
    public readonly TestService Service;
    public readonly int A;
    public readonly bool B;

    [InjectConstructor(typeof(ConstructorObjectResolverIntBool))]
    public ConstructorInjections(int a, bool b)
    {
        A = a;
        B = b;
    }

    [InjectConstructor(typeof(ConstructorObjectResolverStringBuilder))]
    public ConstructorInjections(StringBuilder builder)
    {
        Builder = builder;
    }

    [InjectConstructor(typeof(ConstructorObjectResolver))]
    public ConstructorInjections(int a, bool b, StringBuilder builder)
    {
        A = a;
        B = b;
        Builder = builder;
    }
}

// Resolvers
public class ConstructorObjectResolverIntBool : IResolver
{
    public object Resolve()
    {
        return new ConstructorInjections(3, true);
    }
}

public class ConstructorObjectResolverStringBuilder : IResolver
{
    public object Resolve()
    {
        return new ConstructorInjections(new StringBuilder());
    }
}
public class ConstructorObjectResolver : IResolver<ConstructorInjections>
{
    public void Resolve(Action<ConstructorInjections> callback)
    {
        //do some HTTP request and invoke the callback on response.
        callback.Invoke(new ConstructorInjections(3, true, new StringBuilder()));
    }
}
``` 
So each constructor requires a resolver (examples above a trivial).
Constructor injections do not support static injects, if you want to have singleton access to some object, you can simply inject it into a property or member field with field/property injection.
Note that the third Resolver of the class implements a resolver with a callback.
To actually construct an object using Uniject you can use another extension method for construction:
```c#
var constructioInjections = this.Construct<ConstructorInjections, ConstructorObjectResolverIntBool>();

this.Construct<ConstructorInjections, ConstructorObjectResolver>(constructorInjections =>
{
    // Do something with the constructor object
});
```
## Method Injection
Method injection works very similar to Field and Property injection. It supports both [Inject] and [InjectStatic] with or without return value.
I implemented method injection up tp 4 generic parameters. I know .Net allows up to 10 generic parameters but to me this feels like overkill. I think that 4 params is the absolute maximum number of arguments a method should support.
(Actually 3 arguments is already too many because, if you have 3 arguments, 2 of them are more related to eachother than the thrid meaning they form an object so you can reduce them to 2 arguments.)
Currently method injection does not support resolvers for more control over the invoke process.

See the code below for some examples:
```c#
public class MethodInjection
{
    public int A;
    public StringBuilder Builder;
    public TestService Service;

    //Inject an int into the method 
    [Inject]
    public void IntMethod(int a)
    {
        A = a;
    }
    
    // Inject multiple arguments into the method and return an integer
    [Inject]
    public int IntMethodReturn(int a, StringBuilder s, bool b, float f)
    {
        //do something with all arguments and return a
        return a;
    }
    
    // Inject a static reference to the testservice
    [InjectStatic]
    public void ServiceMethod(TestService service)
    {
        Debug.Log($"service = {service.Id}");
        Service = service;
    }

    // Inject a satic reference to the testservice an integer and return the service
    [InjectStatic]
    public TestService ServiceMethodReturn(TestService service, int a)
    {
        Debug.Log($"service = {service.Id}");
        return service;
    }
}
```
The invoke methods through Uniject you can use the Invoke extension methods:
<i>*Note that you must call the extension method with the correct target object as a reference</i>
```c#
// create the object
var methodInjections = new MethodInjection();

// invoke the IntMethod
methodInjections.Invoke<int>(methodInjections.IntMethod);

// invoke the IntMethodReturn
methodInjections.Invoke<int, int>(methodInjections.IntMethodReturn)

// invoke the ServiceMethod statically
methodInjections.Invoke<TestService>(methodInjections.ServiceMethod);

// invoke the ServiceMethodReturn statically
methodInjections.Invoke<TestService, TestService>(methodInjections.ServiceMethodReturn)
```

# Tests 
See tests [here](Assets/Uniject/Tests)
# ToDO
* Implement resolvers for Method Injection.
* Add an API to access statically injected objects to use them in resolvers.